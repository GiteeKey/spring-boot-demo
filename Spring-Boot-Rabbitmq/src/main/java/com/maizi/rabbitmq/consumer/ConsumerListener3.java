//package com.maizi.rabbitmq.consumer;
//
//import com.rabbitmq.client.Channel;
//import org.springframework.amqp.core.ExchangeTypes;
//import org.springframework.amqp.core.Message;
//import org.springframework.amqp.rabbit.annotation.*;
//import org.springframework.amqp.support.AmqpHeaders;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.messaging.handler.annotation.Header;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//
//@Component
//public class ConsumerListener3 {
//
//    // 队列1 消费者
//    @RabbitListener(bindings = {
//            @QueueBinding(value = @Queue(value = "${spring.rabbitmq.first.queue}", durable = "true",autoDelete = "true"), // 队列名, durable 表示队列是否持久化, autoDelete 表示没有消费者之后队列是否自动删除
//                    exchange = @Exchange(value = "${spring.rabbitmq.first.exchange}", type = ExchangeTypes.DIRECT), // exchange: @Exchange 注解，用于声明 exchange， type 指定消息投递策略，我们这里用的 topic 方式
//                    key = "${spring.rabbitmq.first.rountingKey}")},  // rounting_Key
//            concurrency = "1-4" // 并发消费
//            )
//    @RabbitHandler
//    public void msg1(Message message, Channel channel, String msg) throws IOException {
//
//        long deliveryTag = message.getMessageProperties().getDeliveryTag();
//
//        System.out.println("Thread.currentThread().getName() = " + Thread.currentThread().getName());
//        System.out.println("message3 = " + message);
//        System.out.println("channel3 = " + channel);
//        System.out.println("msg3 = " + msg);
//
//    }
//
//    // 队列2 消费者
//    @RabbitListener(bindings = {
//            @QueueBinding(value = @Queue(value = "${spring.rabbitmq.second.queue}", durable = "true",autoDelete = "true"), // 队列名, durable 表示队列是否持久化, autoDelete 表示没有消费者之后队列是否自动删除
//                    exchange = @Exchange(value = "${spring.rabbitmq.second.exchange}", type = ExchangeTypes.DIRECT), // exchange: @Exchange 注解，用于声明 exchange， type 指定消息投递策略，我们这里用的 topic 方式
//                    key = "${spring.rabbitmq.second.rountingKey}")},  // rounting_Key
//            concurrency = "1-4")  // 并发消费
//    @RabbitHandler
//    public void msg2(Message message, Channel channel, String msg) {
//        System.out.println("Thread.currentThread().getName() = " + Thread.currentThread().getName());
//        System.out.println("message3 = " + message);
//        System.out.println("channel3 = " + channel);
//        System.out.println("msg3 = " + msg);
//    }
//
//}
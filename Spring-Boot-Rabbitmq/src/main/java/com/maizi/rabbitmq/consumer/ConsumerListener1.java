//package com.maizi.rabbitmq.consumer;
//
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
//@Component
//public class ConsumerListener1 {
//
//    @RabbitListener(queues = "${spring.rabbitmq.first.queue}")   // 队列一
//    @RabbitListener(queues = "${spring.rabbitmq.second.queue}")  // 队列二
//    public void msg(String message) {
//        System.out.println("message = " + message);
//    }
//
//}
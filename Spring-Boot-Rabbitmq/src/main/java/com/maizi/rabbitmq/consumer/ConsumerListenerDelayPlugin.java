package com.maizi.rabbitmq.consumer;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;

@Component
public class ConsumerListenerDelayPlugin {

    private static Logger logger = LoggerFactory.getLogger(ConsumerListenerDelayPlugin.class);

    private Integer count = 0;

    @RabbitListener(queues = "delay_queue")
    public void queue1(Message message, Channel channel, String msg) throws IOException {

        logger.info("[延迟插件消费者接收时间]-》接收时间:{},  message:{} , channel1:{}",LocalDateTime.now(),message,channel);

    }

}
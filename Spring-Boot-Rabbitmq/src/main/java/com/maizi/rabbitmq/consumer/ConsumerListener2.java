//package com.maizi.rabbitmq.consumer;
//
//import com.rabbitmq.client.Channel;
//import org.springframework.amqp.core.Message;
//import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
//import org.springframework.stereotype.Component;
//
//
//@Component
//public class ConsumerListener2 implements ChannelAwareMessageListener {
//
//    //重试次数
//    private int retryCount = 0;
//
//    /**
//     * 监听MQ
//     *
//     * @param message
//     * @param channel
//     * @throws Exception
//     */
//    @Override
//    public void onMessage(Message message, Channel channel) throws Exception {
//
//        long deliveryTag = message.getMessageProperties().getDeliveryTag();
//
//        try {
//            System.out.println("message2 = " + message);
//            System.out.println("channel2 = " + channel);
//
//            int a = 1 / 0;
//        } catch (Exception e) {
//            retryCount++;
//            //重新抛出异常  触发重试机制
//            throw new RuntimeException(e);
//        } finally {
//            //重试次数达到限制
//            if (retryCount == 3) {
//                retryCount = 0;
//                System.out.println("处理订单消息异常，nack消息到死信队列");
//                // 不重新入队，发送到死信队列
//                channel.basicNack(deliveryTag, false,false);
//            }
//        }
//
//    }
//}

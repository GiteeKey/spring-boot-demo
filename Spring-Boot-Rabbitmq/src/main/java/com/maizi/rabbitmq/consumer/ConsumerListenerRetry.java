package com.maizi.rabbitmq.consumer;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;

@Component
public class ConsumerListenerRetry {

    private static Logger logger = LoggerFactory.getLogger(ConsumerListenerRetry.class);

    private Integer count = 0;

    @RabbitListener(queues = "${spring.rabbitmq.first.queue}")
    public void queue1(Message message, Channel channel, String msg) throws IOException {

        logger.info("[消费者]-》重试次数:{},  message:{} , channel1:{}",count++,message,channel);

        try {
            int a = 10 / 0;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 配置了 RabbitmqErrorConfig 中 RepublishMessageRecoverer 消息补偿
     * 消息发送失败了  回转发到 此队列
     * 如果没有配置 RepublishMessageRecoverer 则此队列不会接收到消息， 默认是RejectAndDontRequeueRecoverer 重试后将消息丢弃
     * @param message
     * @param channel
     * @param msg
     * @throws IOException
     */
    @RabbitListener(queues = "${spring.rabbitmq.second.queue}")
    public void queue2(Message message, Channel channel, String msg) throws IOException {
        logger.info("[消息重试失败队列] 接收时间：{},  message:{} , channel1:{}", LocalDateTime.now(),message,channel);
    }
}
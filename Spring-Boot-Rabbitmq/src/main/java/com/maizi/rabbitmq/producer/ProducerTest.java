package com.maizi.rabbitmq.producer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProducerTest {

    // 第一个mq连接
    @Resource
    @Qualifier("firstRabbitTemplate")
    private RabbitTemplate firstRabbitTemplate;

    // 第二个mq连接
    @Resource
    @Qualifier("secondRabbitTemplate")
    private RabbitTemplate secondRabbitTemplate;

    // 第一个mq信息
    @Value("${spring.rabbitmq.first.exchange}")
    private String firstExchange;
    @Value("${spring.rabbitmq.first.queue}")
    private String firstQueue;
    @Value("${spring.rabbitmq.first.rountingKey}")
    private String firstRountingKey;

    // 第二个mq信息
    @Value("${spring.rabbitmq.second.exchange}")
    private String secondExchange;
    @Value("${spring.rabbitmq.second.queue}")
    private String secondQueue;
    @Value("${spring.rabbitmq.second.rountingKey}")
    private String secondRountingKey;

    @Test
    public void sendRouting() {
        for (int i = 0; i < 10; i++) {
            firstRabbitTemplate.convertAndSend(firstExchange, firstRountingKey, "rabbitmq-01-消息");
            secondRabbitTemplate.convertAndSend(secondExchange, secondRountingKey, "rabbitmq-02-消息");
        }
    }


    /**
     * 利用 RabbitMQ插件 x-delay-message 实现队列延迟发送 - 生产者
     */
    @Test
    public void sendDelayPlugin() {

        MessagePostProcessor postProcessor = new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                // 延迟时间  毫秒  30000 毫秒 = 30秒
                message.getMessageProperties().setHeader(MessageProperties.X_DELAY, 30000);
                return message;
            }
        };

        System.out.println("延迟插件生产者发送时间" + LocalDateTime.now());
        // 发送
        firstRabbitTemplate.convertAndSend("delay_exchange", "delay_routing_key", "利用 RabbitMQ插件 x-delay-message 实现队列延迟发送",postProcessor);
    }

}

package com.maizi.rabbitmq.config;

//import com.maizi.rabbitmq.consumer.ConsumerListener2;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;

/**
 * 配置双队列
 */
@Configuration
public class RabbitPlusConfig {

    // 如果报错  org.springframework.amqp.rabbit.listener.exception.ListenerExecutionFailedException: Listener threw exception
    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }



    /* ============================================================ 队列一 start ============================================================  */

    /**
     * 配置 rabbitmq-1 连接
     *
     * @param host        IP
     * @param port        端口号
     * @param username    用户名
     * @param password    密码
     * @param virtualHost 主机/路由
     * @return
     */
    @Bean(name = "firstConnectionFactory")
    @Primary
    public ConnectionFactory firstConnectionFactory(
            @Value("${spring.rabbitmq.first.host}") String host,
            @Value("${spring.rabbitmq.first.port}") Integer port,
            @Value("${spring.rabbitmq.first.username}") String username,
            @Value("${spring.rabbitmq.first.password}") String password,
            @Value("${spring.rabbitmq.first.virtual-host}") String virtualHost
    ) {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost(host);
        connectionFactory.setPort(port);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setVirtualHost(virtualHost);

        // todo 发送方确认模式  true 为开启，false 为关闭
        connectionFactory.setPublisherConfirms(true);
        return connectionFactory;
    }

    @Bean(name = "firstRabbitTemplate")
    @Primary
    public RabbitTemplate firstRabbitTemplate(
            @Qualifier("firstConnectionFactory") ConnectionFactory connectionFactory
    ) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());

        // 发送方确认 回掉函数 （必须设置 connectionFactory.setPublisherConfirms(true) ））
        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            System.out.println("==================== 发送发确认回调函数 ====================");
            System.out.println("first ConfirmCallback-发送发确认   相关数据：" + correlationData);
            System.out.println("first ConfirmCallback-发送发确认   确认情况：" + ack);
            System.out.println("first ConfirmCallback-发送发确认   原因：" + cause);
        });

        // 消息成功路由到队列 -> 开始失败通知
        rabbitTemplate.setMandatory(true);
        // 消息成功路由到队列 -> 路由失败通知回调函数
        rabbitTemplate.setReturnCallback((message, replyCode, replyText, exchange, routingKey) -> {
            System.out.println("==================== 无法路由消息 ====================");
            System.out.println("first ReturnCallback-路由失败通知   消息：" + message);
            System.out.println("first ReturnCallback-路由失败通知   回应码：" + replyCode);
            System.out.println("first ReturnCallback-路由失败通知   回应信息：" + replyText);
            System.out.println("first ReturnCallback-路由失败通知   交换机：" + exchange);
            System.out.println("first ReturnCallback-路由失败通知   路由键：" + routingKey);
        });

        return rabbitTemplate;
    }

    @Bean(name = "firstFactory")
    public SimpleRabbitListenerContainerFactory firstFactory(
            SimpleRabbitListenerContainerFactoryConfigurer configurer,
            @Qualifier("firstConnectionFactory") ConnectionFactory connectionFactory
    ) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(jsonMessageConverter());
//        factory.setAcknowledgeMode(AcknowledgeMode.MANUAL);             //开启手动 ack
        return factory;
    }

    // 交换机 - exchange
    @Value("${spring.rabbitmq.first.exchange}")
    private String firstExchange;
    // 队列 - queue
    @Value("${spring.rabbitmq.first.queue}")
    private String firstQueue;
    // 路由- routingKey
    @Value("${spring.rabbitmq.first.rountingKey}")
    private String firstRoutingKey;

    // 创建交换机
    @Bean
    public DirectExchange createFirstDirectExchange() {
        return new DirectExchange(firstExchange);
    }

    // 创建队列
    @Bean
    public Queue createFirstQueue() {
        return new Queue(firstQueue);
    }

    // 绑定(队列，交换机，rotingKey)
    @Bean
    public Binding bindingFirstDirect() {
        return BindingBuilder.bind(createFirstQueue()).to(createFirstDirectExchange()).with(firstRoutingKey);
    }

    /* ============================================================ 队列一 end ============================================================  */


    /* ============================================================ 队列二 start ============================================================  */


    /**
     * 配置 rabbitmq-2 连接
     *
     * @param host        IP
     * @param port        端口号
     * @param username    用户名
     * @param password    密码
     * @param virtualHost 主机/路由
     * @return
     */
    @Bean(name = "secondConnectionFactory")
    public ConnectionFactory secondConnectionFactory(
            @Value("${spring.rabbitmq.second.host}") String host,
            @Value("${spring.rabbitmq.second.port}") int port,
            @Value("${spring.rabbitmq.second.username}") String username,
            @Value("${spring.rabbitmq.second.password}") String password,
            @Value("${spring.rabbitmq.second.virtual-host}") String virtualHost
    ) {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost(host);
        connectionFactory.setPort(port);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setVirtualHost(virtualHost);

        // todo 发送方确认模式  true 为开启，false 为关闭
        connectionFactory.setPublisherConfirms(true);
        return connectionFactory;
    }


    @Bean(name = "secondRabbitTemplate")
    public RabbitTemplate secondRabbitTemplate(
            @Qualifier("secondConnectionFactory") ConnectionFactory connectionFactory
    ) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        // 发送方确认 回掉函数 （必须设置 connectionFactory.setPublisherConfirms(true) ）
        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            System.out.println("==================== 发送发确认回调函数 ====================");
            System.out.println("second ConfirmCallback-发送发确认   相关数据：" + correlationData);
            System.out.println("second ConfirmCallback-发送发确认   确认情况：" + ack);
            System.out.println("second ConfirmCallback-发送发确认   原因：" + cause);
        });

        // 消息成功路由到队列 -> 开始失败通知
        rabbitTemplate.setMandatory(true);
        // 消息成功路由到队列 -> 路由失败通知回调函数
        rabbitTemplate.setReturnCallback((message, replyCode, replyText, exchange, routingKey) -> {
            System.out.println("==================== 无法路由消息 ====================");
            System.out.println("second ReturnCallback-路由失败通知   消息：" + message);
            System.out.println("second ReturnCallback-路由失败通知   回应码：" + replyCode);
            System.out.println("second ReturnCallback-路由失败通知   回应信息：" + replyText);
            System.out.println("second ReturnCallback-路由失败通知   交换机：" + exchange);
            System.out.println("second ReturnCallback-路由失败通知   路由键：" + routingKey);
        });
        return rabbitTemplate;
    }


    @Bean(name = "secondFactory")
    public SimpleRabbitListenerContainerFactory secondFactory(
            SimpleRabbitListenerContainerFactoryConfigurer configurer,
            @Qualifier("secondConnectionFactory") ConnectionFactory connectionFactory
    ) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(jsonMessageConverter());
//        factory.setAcknowledgeMode(AcknowledgeMode.MANUAL);             //开启手动 ack
        return factory;
    }

    // 交换机 - exchange
    @Value("${spring.rabbitmq.second.exchange}")
    private String secondExchange;
    // 队列 - queue
    @Value("${spring.rabbitmq.second.queue}")
    private String secondQueue;
    // 路由- routingKey
    @Value("${spring.rabbitmq.second.rountingKey}")
    private String secondRoutingKey;


    // 创建交换机
    @Bean
    public DirectExchange createSecondDirectExchange() {
        return new DirectExchange(secondExchange);
    }

    // 创建队列
    @Bean
    public Queue createSecondQueue() {
        return new Queue(secondQueue);
    }

    // 绑定(队列，交换机，rotingKey)
    @Bean
    public Binding bindingSecondDirect() {
        return BindingBuilder.bind(createSecondQueue()).to(createSecondDirectExchange()).with(secondRoutingKey);
    }

    /* ============================================================ 队列二 start ============================================================  */

    /**
     * 自定义消费处理
     *
     * @param receiver          监听处理类
     * @param connectionFactory 连接工厂
     */
//    @Bean
//    public SimpleMessageListenerContainer modbusMessageContainer(ConsumerListener2 receiver, ConnectionFactory connectionFactory) {
//        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
//        container.setQueueNames(firstQueue, secondQueue);
//        container.setExposeListenerChannel(true);
////        container.setAcknowledgeMode(AcknowledgeMode.MANUAL);//设置确认模式为手工确认
//        container.setMessageListener(receiver);//监听处理类
//        return container;
//    }

}
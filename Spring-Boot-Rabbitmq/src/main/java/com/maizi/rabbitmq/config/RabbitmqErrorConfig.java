package com.maizi.rabbitmq.config;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.retry.ImmediateRequeueMessageRecoverer;
import org.springframework.amqp.rabbit.retry.MessageRecoverer;
import org.springframework.amqp.rabbit.retry.RejectAndDontRequeueRecoverer;
import org.springframework.amqp.rabbit.retry.RepublishMessageRecoverer;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 消息补偿队列 配置
 */
@Configuration
public class RabbitmqErrorConfig {

    // 交换机 - exchange
    @Value("${spring.rabbitmq.second.exchange}")
    private String secondExchange;
    // 队列 - queue
    @Value("${spring.rabbitmq.second.queue}")
    private String secondQueue;
    // 路由- routingKey
    @Value("${spring.rabbitmq.second.rountingKey}")
    private String secondRoutingKey;

    /**
     * 补偿队列
     * RepublishMessageRecoverer   补偿队列 （创建一个异常队列，重试完成后将消息发送到异常队列）
     * ImmediateRequeueMessageRecoverer 重新发回队列 (重试完成后,返回队列,然后在次重试，周而复始，这样会影响后续的消息消费）
     * RejectAndDontRequeueRecoverer 重试后将消息丢弃（默认）
     *
     * @param rabbitTemplate
     * @return
     */
    @Bean
    public MessageRecoverer messageRecoverer(@Qualifier(value = "secondRabbitTemplate") RabbitTemplate rabbitTemplate) {
        return new RepublishMessageRecoverer(rabbitTemplate, secondExchange, secondRoutingKey);  // 补偿队列 （创建一个异常队列，重试完成后将消息发送到异常队列）
//        return new ImmediateRequeueMessageRecoverer();    // 重新发回队列
//        return new RejectAndDontRequeueRecoverer();    // 重试后将消息丢弃（默认）
    }
}

package com.maizi.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * 利用 RabbitMQ插件 x-delay-message 实现队列延迟发送
 */
@Configuration
public class RabbitDelayPlugin {

    private static final String EXCHANGE_NAME = "delay_exchange";
    private static final String QUEUE_NAME = "delay_queue";
    private static final String ROUTING_KEY = "delay_routing_key";

    // 创建交换机
    @Bean
    public CustomExchange createDelayExchange() {
        HashMap<String, Object> map = new HashMap<>(1);
        map.put("x-delayed-type", "direct");
        return new CustomExchange(EXCHANGE_NAME,"x-delayed-message",true,false,map);
    }

    // 创建队列
    @Bean
    public Queue createDelayQueue() {
        // queue 队列名, durable 是否持久化, exclusive 是否排它, autoDelete 是否自动删除
        return new Queue(QUEUE_NAME,true,false,false);
    }

    // 绑定(队列，交换机，rotingKey)
    @Bean
    public Binding bindingDelayDirect() {
        return BindingBuilder.bind(createDelayQueue()).to(createDelayExchange()).with(ROUTING_KEY).noargs();
    }

}
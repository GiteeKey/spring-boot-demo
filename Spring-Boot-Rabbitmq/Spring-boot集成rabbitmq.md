## 0.介绍

[参考文章](https://zhuanlan.zhihu.com/p/145336656?utm_id=0)

### 基本概念

queue：队列，每个队列可以有多个消费者，但是一条消息只会被一个消费者消费

exchange:交换机，队列可以绑定交换机，交换机根据路由或者其他匹配信息将消息发送至queue

### 模式介绍

simple模式：不需要交换机，直连模式。一个队列只有一个消费者

work模式：一个队列多个消费者

direct模式：需要交换机，通过交换机的路由key，精确匹配queue，并发送至对应的queue

topic模式：通过路由与路由key，模糊匹配的模式。可用通配符。比如key.1会被绑定路由key.*的queue获取到

fanout: 广播模式，不需要路由key，给所有绑定到交换机的queue

## 1.依赖

```properties
  <!--    SpringBoot父依赖-->
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.1.6.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>

    <dependencies>
        <!--       web依赖-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <!--springBoot测试依赖-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-amqp -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-amqp</artifactId>
        </dependency>

        <!--rabbit测试依赖-->
        <dependency>
            <groupId>org.springframework.amqp</groupId>
            <artifactId>spring-rabbit-test</artifactId>
        </dependency>

    </dependencies>
```

## 2.application

```properties
# 端口号
server.port=9100
# 服务名
spring.application.name=spring-boot-rabbitmq

# rabbitmq 连接一  (IP，端口号，用户名，密码，virtual-host主机，exchange交换机，队列，rountingKey)
spring.rabbitmq.first.host=127.0.0.1
spring.rabbitmq.first.port=5672
spring.rabbitmq.first.username=admin
spring.rabbitmq.first.password=admin
spring.rabbitmq.first.virtual-host=/pnr
spring.rabbitmq.first.exchange=TEST.EXCHANGE_1
spring.rabbitmq.first.queue=TEST.QUEUE_1
spring.rabbitmq.first.rountingKey=TEST.KEY_1

# rabbitmq 连接二  (IP，端口号，用户名，密码，virtual-host主机，exchange交换机，队列，rountingKey)
spring.rabbitmq.second.host=127.0.0.1
spring.rabbitmq.second.port=5672
spring.rabbitmq.second.username=admin
spring.rabbitmq.second.password=admin
spring.rabbitmq.second.virtual-host=/pnr
spring.rabbitmq.second.exchange=TEST.EXCHANGE_2
spring.rabbitmq.second.queue=TEST.QUEUE_2
spring.rabbitmq.second.rountingKey=TEST.KEY_2
```

## 3.config

```java
package com.maizi.rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class RabbitPlusConfig {



    /* ============================================================ 队列一 start ============================================================  */

    /**
     * 配置 rabbitmq-1 连接
     *
     * @param host        IP
     * @param port        端口号
     * @param username    用户名
     * @param password    密码
     * @param virtualHost 主机/路由
     * @return
     */
    @Bean(name = "firstConnectionFactory")
    @Primary
    public ConnectionFactory firstConnectionFactory(
            @Value("${spring.rabbitmq.first.host}") String host,
            @Value("${spring.rabbitmq.first.port}") Integer port,
            @Value("${spring.rabbitmq.first.username}") String username,
            @Value("${spring.rabbitmq.first.password}") String password,
            @Value("${spring.rabbitmq.first.virtual-host}") String virtualHost
    ) {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost(host);
        connectionFactory.setPort(port);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setVirtualHost(virtualHost);

        // todo 发送方确认模式  true 为开启，false 为关闭
        connectionFactory.setPublisherConfirms(true);
        return connectionFactory;
    }

    @Bean(name = "firstRabbitTemplate")
    @Primary
    public RabbitTemplate firstRabbitTemplate(
            @Qualifier("firstConnectionFactory") ConnectionFactory connectionFactory
    ) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);

        // 发送方确认 回掉函数 （必须设置 connectionFactory.setPublisherConfirms(true) ））
        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            System.out.println("first ConfirmCallback-发送发确认   相关数据：" + correlationData);
            System.out.println("first ConfirmCallback-发送发确认   确认情况：" + ack);
            System.out.println("first ConfirmCallback-发送发确认   原因：" + cause);
        });

        // 消息成功路由到队列 -> 开始失败通知
        rabbitTemplate.setMandatory(true);
        // 消息成功路由到队列 -> 路由失败通知回调函数
        rabbitTemplate.setReturnCallback((message, replyCode, replyText, exchange, routingKey) -> {
            System.out.println("==================== 无法路由消息 ====================");
            System.out.println("first ReturnCallback-路由失败通知   消息：" + message);
            System.out.println("first ReturnCallback-路由失败通知   回应码：" + replyCode);
            System.out.println("first ReturnCallback-路由失败通知   回应信息：" + replyText);
            System.out.println("first ReturnCallback-路由失败通知   交换机：" + exchange);
            System.out.println("first ReturnCallback-路由失败通知   路由键：" + routingKey);
        });

        return rabbitTemplate;
    }

    @Bean(name = "firstFactory")
    public SimpleRabbitListenerContainerFactory firstFactory(
            SimpleRabbitListenerContainerFactoryConfigurer configurer,
            @Qualifier("firstConnectionFactory") ConnectionFactory connectionFactory
    ) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        return factory;
    }

    // 交换机 - exchange
    @Value("${spring.rabbitmq.first.exchange}")
    private String firstExchange;
    // 队列 - queue
    @Value("${spring.rabbitmq.first.queue}")
    private String firstQueue;
    // 路由- routingKey
    @Value("${spring.rabbitmq.first.rountingKey}")
    private String firstRoutingKey;
    // 创建交换机
    @Bean
    public DirectExchange createFirstDirectExchange() {
        return new DirectExchange(firstExchange);
    }
    // 创建队列
    @Bean
    public Queue createFirstQueue(){
        return new Queue(firstQueue);
    }
    // 绑定(队列，交换机，rotingKey)
    @Bean
    public Binding bindingFirstDirect() {
        return BindingBuilder.bind(createFirstQueue()).to(createFirstDirectExchange()).with(firstRoutingKey);
    }

    /* ============================================================ 队列一 end ============================================================  */


    /* ============================================================ 队列二 start ============================================================  */


    /**
     * 配置 rabbitmq-2 连接
     *
     * @param host        IP
     * @param port        端口号
     * @param username    用户名
     * @param password    密码
     * @param virtualHost 主机/路由
     * @return
     */
    @Bean(name = "secondConnectionFactory")
    public ConnectionFactory secondConnectionFactory(
            @Value("${spring.rabbitmq.second.host}") String host,
            @Value("${spring.rabbitmq.second.port}") int port,
            @Value("${spring.rabbitmq.second.username}") String username,
            @Value("${spring.rabbitmq.second.password}") String password,
            @Value("${spring.rabbitmq.second.virtual-host}") String virtualHost
    ) {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost(host);
        connectionFactory.setPort(port);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setVirtualHost(virtualHost);

        // todo 发送方确认模式  true 为开启，false 为关闭
        connectionFactory.setPublisherConfirms(true);
        return connectionFactory;
    }


    @Bean(name = "secondRabbitTemplate")
    public RabbitTemplate secondRabbitTemplate(
            @Qualifier("secondConnectionFactory") ConnectionFactory connectionFactory
    ) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);

        // 发送方确认 回掉函数 （必须设置 connectionFactory.setPublisherConfirms(true) ）
        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            System.out.println("==================== 发送发确认回调函数 ====================");
            System.out.println("second ConfirmCallback-发送发确认   相关数据：" + correlationData);
            System.out.println("second ConfirmCallback-发送发确认   确认情况：" + ack);
            System.out.println("second ConfirmCallback-发送发确认   原因：" + cause);
        });

        // 消息成功路由到队列 -> 开始失败通知
        rabbitTemplate.setMandatory(true);
        // 消息成功路由到队列 -> 路由失败通知回调函数
        rabbitTemplate.setReturnCallback((message, replyCode, replyText, exchange, routingKey) -> {
            System.out.println("==================== 无法路由消息 ====================");
            System.out.println("second ReturnCallback-路由失败通知   消息：" + message);
            System.out.println("second ReturnCallback-路由失败通知   回应码：" + replyCode);
            System.out.println("second ReturnCallback-路由失败通知   回应信息：" + replyText);
            System.out.println("second ReturnCallback-路由失败通知   交换机：" + exchange);
            System.out.println("second ReturnCallback-路由失败通知   路由键：" + routingKey);
        });
        return rabbitTemplate;
    }


    @Bean(name = "secondFactory")
    public SimpleRabbitListenerContainerFactory secondFactory(
            SimpleRabbitListenerContainerFactoryConfigurer configurer,
            @Qualifier("secondConnectionFactory") ConnectionFactory connectionFactory
    ) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        return factory;
    }

    // 交换机 - exchange
    @Value("${spring.rabbitmq.second.exchange}")
    private String secondExchange;
    // 队列 - queue
    @Value("${spring.rabbitmq.second.queue}")
    private String secondQueue;
    // 路由- routingKey
    @Value("${spring.rabbitmq.second.rountingKey}")
    private String secondRoutingKey;

    // 创建交换机
    @Bean
    public DirectExchange createSecondDirectExchange() {
        return new DirectExchange(secondExchange);
    }

    // 创建队列
    @Bean
    public Queue createSecondQueue(){
        return new Queue(secondQueue);
    }

    // 绑定(队列，交换机，rotingKey)
    @Bean
    public Binding bindingSecondDirect() {
        return BindingBuilder.bind(createSecondQueue()).to(createSecondDirectExchange()).with(secondRoutingKey);
    }

    /* ============================================================ 队列二 start ============================================================  */

}
```

## 生产者测试

```java

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProducerTest {

    // 第一个mq连接
    @Resource
    @Qualifier("firstRabbitTemplate")
    private RabbitTemplate firstRabbitTemplate;

    // 第二个mq连接
    @Resource
    @Qualifier("secondRabbitTemplate")
    private RabbitTemplate secondRabbitTemplate;

    // 第一个mq信息
    @Value("${spring.rabbitmq.first.exchange}")
    private String firstExchange;
    @Value("${spring.rabbitmq.first.queue}")
    private String firstQueue;
    @Value("${spring.rabbitmq.first.rountingKey}")
    private String firstRountingKey;

    // 第二个mq信息
    @Value("${spring.rabbitmq.second.exchange}")
    private String secondExchange;
    @Value("${spring.rabbitmq.second.queue}")
    private String secondQueue;
    @Value("${spring.rabbitmq.second.rountingKey}")
    private String secondRountingKey;

    @Test
    public void sendRoutingLounge() {
        firstRabbitTemplate.convertAndSend(firstExchange, firstRountingKey, "rabbitmq-01-消息");
        secondRabbitTemplate.convertAndSend(secondExchange, secondRountingKey, "rabbitmq-02-消息");
    }

}

```

## 消费者测试

### 测试方式一

```java
@Component
public class ConsumerListener1 {

    @RabbitListener(queues = "${spring.rabbitmq.first.queue}")   // 队列一
    @RabbitListener(queues = "${spring.rabbitmq.second.queue}")  // 队列二
    public void msg(String message) {
        System.out.println("message = " + message);
    }

}
```

### 测试方式二

**在config中声明bean**

```java
    /**
     * 自定义消费处理
     *
     * @param receiver          监听处理类
     * @param connectionFactory 连接工厂
     */
    @Bean
    public SimpleMessageListenerContainer modbusMessageContainer(ConsumerListener2 receiver, ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        container.setQueueNames(firstQueue, secondQueue);
        container.setExposeListenerChannel(true);
        //container.setAcknowledgeMode(AcknowledgeMode.MANUAL);//设置确认模式为手工确认
        container.setMessageListener(receiver);//监听处理类
        return container;
    }
```

**监听**

```java

@Component
public class ConsumerListener2 implements ChannelAwareMessageListener {

    /**
     * 监听MQ
     *
     * @param message
     * @param channel
     * @throws Exception
     */
    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        System.out.println("message = " + message);
        System.out.println("channel = " + channel);
    }
}
```

### 测试方式三

```java
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

@Component
public class ConsumerListener3 {

    // 队列1 消费者
    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue(value = "${spring.rabbitmq.first.queue}", durable = "true",autoDelete = "true"), // 队列名, durable 表示队列是否持久化, autoDelete 表示没有消费者之后队列是否自动删除
                    exchange = @Exchange(value = "${spring.rabbitmq.first.exchange}", type = ExchangeTypes.DIRECT), // exchange: @Exchange 注解，用于声明 exchange， type 指定消息投递策略，我们这里用的 topic 方式
                    key = "${spring.rabbitmq.first.rountingKey}")},  // rounting_Key
            concurrency = "1-4")  // 并发消费
    @RabbitHandler
    public void msg1(Message message, Channel channel, String msg) {
        System.out.println("Thread.currentThread().getName() = " + Thread.currentThread().getName());
        System.out.println("message3 = " + message);
        System.out.println("channel3 = " + channel);
        System.out.println("msg3 = " + msg);
    }

    // 队列2 消费者
    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue(value = "${spring.rabbitmq.second.queue}", durable = "true",autoDelete = "true"), // 队列名, durable 表示队列是否持久化, autoDelete 表示没有消费者之后队列是否自动删除
                    exchange = @Exchange(value = "${spring.rabbitmq.second.exchange}", type = ExchangeTypes.DIRECT), // exchange: @Exchange 注解，用于声明 exchange， type 指定消息投递策略，我们这里用的 topic 方式
                    key = "${spring.rabbitmq.second.rountingKey}")},  // rounting_Key
            concurrency = "1-4")  // 并发消费
    @RabbitHandler
    public void msg2(Message message, Channel channel, String msg) {
        System.out.println("Thread.currentThread().getName() = " + Thread.currentThread().getName());
        System.out.println("message3 = " + message);
        System.out.println("channel3 = " + channel);
        System.out.println("msg3 = " + msg);
    }

}
```
## 4.消费者确认

```properties
# 消费者确认机制
#  none：不处理。即消息投递给消费者后立刻ack，消息会立刻从MQ删除。非常不安全，不建议使用
#  manual： 手动模式。需要自己在业务代码中调用api，发送ack或reject，存在业务入侵，但更灵活
#  auto：自动模式。SpringAMQP利用AOP对我们的消息处理逻辑做了环绕增强，当业务正常执行时则自动返回ack
spring.rabbitmq.listener.direct.acknowledge-mode=manual

# 或者在config中配置
factory.setAcknowledgeMode(AcknowledgeMode.MANUAL);             //开启手动 ack
```
```java
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ConsumerListener1 {

    @RabbitListener(queues = "${spring.rabbitmq.first.queue}")
    @RabbitListener(queues = "${spring.rabbitmq.second.queue}")
    public void msg(Message message, Channel channel,String msg) throws IOException {
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        System.out.println("message1 = " + message);
        System.out.println("channel1 = " + channel);
        System.out.println("msg1 = " + msg);

        try {
            int a = 0/0;
            // 手动ack，deliveryTag表示消息的唯一标志，multiple表示是否是批量确认
            channel.basicAck(deliveryTag, false);
        } catch (IOException e) {
            
            // 手动nack，告诉broker消费者处理失败，deliveryTag表示消息的唯一标志，multiple表示是否是批量确认,最后一个参数表示是否需要将消息重新入列
            channel.basicNack(deliveryTag, false,false);

            //手动拒绝消息。deliveryTag表示消息的唯一标志，第二个参数表示是否重新入列
//            channel.basicReject(deliveryTag, false);
        }
    }

}
```

## 5.重试

### application配置

```properties
# 消费者确认机制
#  none：不处理。即消息投递给消费者后立刻ack，消息会立刻从MQ删除。非常不安全，不建议使用
#  manual： 手动模式。需要自己在业务代码中调用api，发送ack或reject，存在业务入侵，但更灵活
#  auto：自动模式。SpringAMQP利用AOP对我们的消息处理逻辑做了环绕增强，当业务正常执行时则自动返回ack
spring.rabbitmq.listener.simple.acknowledge-mode=auto


# 重试配置
#   最大重试次数8 & 重试间隔1秒 & 间隔时间乘以2 & 最大间隔时间60秒
#   初次消费
#   第一次 1秒
#   第二次 1 * 2 = 2秒
#   第三次 2 * 2 = 4秒
#   第四次 4 * 2 = 8秒
#   第五次 8 * 2 = 16秒
#   第六次 16 * 2 = 32秒
#   第起次 32 * 2 = 64秒 (由于设置最大间隔时间，因此这里为50秒)
# 是否开启重启  true开启
spring.rabbitmq.listener.simple.retry.enabled=true
# 最大重试次数 默认为3
spring.rabbitmq.listener.simple.retry.max-attempts=4
# 重试最大间隔时间,默认为10000ms , 这里配置的是50000ms=50秒
spring.rabbitmq.listener.simple.retry.max-interval=50000
# 传递消息的时间间隔 默认1s
spring.rabbitmq.listener.simple.retry.initial-interval=1000
# 间隔时间乘子，间隔时间 * 乘子 = 下一次的间隔时间，最大不能超过设定的 max-interval
spring.rabbitmq.listener.simple.retry.multiplier=2
```

### 代码

```java
package com.maizi.rabbitmq.consumer;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;

@Component
public class ConsumerListenerRetry {

    private static Logger logger = LoggerFactory.getLogger(ConsumerListenerRetry.class);

    private Integer count = 0;

    @RabbitListener(queues = "${spring.rabbitmq.first.queue}")
    public void queue1(Message message, Channel channel, String msg) throws IOException {

        logger.info("[消费者]-》重试次数:{},  message:{} , channel1:{}",count++,message,channel);

        try {
            int a = 10 / 0;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 配置了 RabbitmqErrorConfig 中 RepublishMessageRecoverer 消息补偿
     * 消息发送失败了  会转发到 此队列
     * 如果没有配置 RepublishMessageRecoverer 则此队列不会接收到消息， 默认是RejectAndDontRequeueRecoverer 重试后将消息丢弃
     */
    @RabbitListener(queues = "${spring.rabbitmq.second.queue}")
    public void queue2(Message message, Channel channel, String msg) throws IOException {
        logger.info("[消息重试失败队列] 接收时间：{},  message:{} , channel1:{}", LocalDateTime.now(),message,channel);
    }
}
```

### 消息补偿配置

RepublishMessageRecoverer   补偿队列 （创建一个异常队列，重试完成后将消息发送到异常队列）

ImmediateRequeueMessageRecoverer 重新发回队列 (重试完成后,返回队列,然后在次重试，周而复始，这样会影响后续的消息消费）

RejectAndDontRequeueRecoverer 重试后将消息丢弃（默认）

**当重试3次后，还是不成功，将触发补尝队列**

```java
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.retry.ImmediateRequeueMessageRecoverer;
import org.springframework.amqp.rabbit.retry.MessageRecoverer;
import org.springframework.amqp.rabbit.retry.RejectAndDontRequeueRecoverer;
import org.springframework.amqp.rabbit.retry.RepublishMessageRecoverer;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 消息补偿队列 配置
 */
@Configuration
public class RabbitmqErrorConfig {

    // 交换机 - exchange
    @Value("${spring.rabbitmq.second.exchange}")
    private String secondExchange;
    // 队列 - queue
    @Value("${spring.rabbitmq.second.queue}")
    private String secondQueue;
    // 路由- routingKey
    @Value("${spring.rabbitmq.second.rountingKey}")
    private String secondRoutingKey;

    /**
     * 补偿队列
     * RepublishMessageRecoverer   补偿队列 （创建一个异常队列，重试完成后将消息发送到异常队列）
     * ImmediateRequeueMessageRecoverer 重新发回队列 (重试完成后,返回队列,然后在次重试，周而复始，这样会影响后续的消息消费）
     * RejectAndDontRequeueRecoverer 重试后将消息丢弃（默认）
     *
     * @param rabbitTemplate
     * @return
     */
    @Bean
    public MessageRecoverer messageRecoverer(@Qualifier(value = "secondRabbitTemplate") RabbitTemplate rabbitTemplate) {
        return new RepublishMessageRecoverer(rabbitTemplate, secondExchange, secondRoutingKey);  // 补偿队列 （创建一个异常队列，重试完成后将消息发送到异常队列）
//        return new ImmediateRequeueMessageRecoverer();    // 重新发回队列
//        return new RejectAndDontRequeueRecoverer();    // 重试后将消息丢弃（默认）
    }
}

```

## 6.延迟发送

### 方式一

<font color="red">利用 RabbitMQ插件 x-delay-message</font>

#### config配置(队列/交换机)

```java
package com.maizi.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * 利用 RabbitMQ插件 x-delay-message 实现队列延迟发送
 */
@Configuration
public class RabbitDelayPlugin {

    private static final String EXCHANGE_NAME = "delay_exchange";
    private static final String QUEUE_NAME = "delay_queue";
    private static final String ROUTING_KEY = "delay_routing_key";

    // 创建交换机
    @Bean
    public CustomExchange createDelayExchange() {
        HashMap<String, Object> map = new HashMap<>(1);
        map.put("x-delayed-type", "direct");
        return new CustomExchange(EXCHANGE_NAME,"x-delayed-message",true,false,map);
    }

    // 创建队列
    @Bean
    public Queue createDelayQueue() {
        // queue 队列名, durable 是否持久化, exclusive 是否排它, autoDelete 是否自动删除
        return new Queue(QUEUE_NAME,true,false,false);
    }

    // 绑定(队列，交换机，rotingKey)
    @Bean
    public Binding bindingDelayDirect() {
        return BindingBuilder.bind(createDelayQueue()).to(createDelayExchange()).with(ROUTING_KEY).noargs();
    }

}
```

#### 生产者

```java
    /**
     * 利用 RabbitMQ插件 x-delay-message 实现队列延迟发送 - 生产者
     */
    @Test
    public void sendDelayPlugin() {

        MessagePostProcessor postProcessor = new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                // 延迟时间  毫秒  30000 毫秒 = 30秒
                message.getMessageProperties().setHeader(MessageProperties.X_DELAY, 30000);
                return message;
            }
        };

        System.out.println("延迟插件生产者发送时间" + LocalDateTime.now());
        // 发送
        firstRabbitTemplate.convertAndSend("delay_exchange", "delay_routing_key", "利用 RabbitMQ插件 x-delay-message 实现队列延迟发送",postProcessor);
    }
```

#### 消费者

```java
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;

@Component
public class ConsumerListenerDelayPlugin {

    private static Logger logger = LoggerFactory.getLogger(ConsumerListenerDelayPlugin.class);

    private Integer count = 0;

    @RabbitListener(queues = "delay_queue")
    public void queue1(Message message, Channel channel, String msg) throws IOException {

        logger.info("[延迟插件消费者接收时间]-》接收时间:{},  message:{} , channel1:{}",LocalDateTime.now(),message,channel);

    }

}
```






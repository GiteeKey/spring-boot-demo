import axios from "axios";
import router from "@/router";
import Element from "element-ui"

axios.defaults.daseURL = "http://localhost:8081"

const request = axios.create({
    timeout: 5000,  //超时时间
    headers: {
        'Content-Type': 'application/json; charset=utf-8' //  返回的JSON类型数据
    }
})

// 请求的拦截，查看是否有token
request.interceptors.request.use(config => {
    config.headers['Authorization'] = localStorage.getItem("token") // 请求头带上token
    return config
})
// 返回的结果
request.interceptors.response.use(response => {
    let res = response.data;
    console.log("response")
    console.log(res)
    if (res.code === 200) {
        return response
    } else {
        Element.Message.error(!res.msg ? '系统异常' : res.msg, {duration: 3 * 1000})
        return Promise.reject(response.data.msg)
    }
}, error => {   // 异常的情况
    console.log(error)
    if (error.response.data) {
        error.message = error.response.data.msg
    }
    // 401没有权限
    if (error.response.status === 401) {
        router.push("/login")
    }
    Element.Message.error(error.message, {duration: 3 * 1000})
    return Promise.reject(error)
})
export default request

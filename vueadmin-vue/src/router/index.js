import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Index from '@/views/index'
import User from "@/views/system/User";
import Role from "@/views/system/Role";
import Menu from "@/views/system/Menu";
import axios from "axios";
import store from "@/store"

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        children: [
            {
                path: '/index',
                name: 'Index',
                component: Index
            },
            {
                path: '/userCenter',
                name: 'UserCenter',
                component: () => import("@/views/system/UserCenter")
            },
            // {
            //     path: '/users',
            //     name: 'SysUser',
            //     component: User
            // },
            // {
            //     path: '/roles',
            //     name: 'SysRole',
            //     component: Role
            // },
            // {
            //     path: '/menus',
            //     name: 'SysMenu',
            //     component: Menu
            // },
        ]
    },
    {
        path: '/login',
        name: 'Login',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        // component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')  方式一
        component: Login  //方式二
    },

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach(((to, from, next) => {

    let hasRoute = store.state.menus.hasRoute
    console.log("hasRoute", hasRoute)
    // 当它没有的时候获取菜单
    if (!hasRoute) {
        // 获取菜单的方法，并校验token(token是之前存到localStorage中的)
        axios.get("/sys/menuList", {
            headers: {
                Authorization: localStorage.getItem("token")
            }
        }).then(res => {
            // 拿到menuList
            store.commit("setMenuList", res.data.data.nav)
            // 拿到用户权限
            store.commit("setPermList", res.data.data.authoritys)
            // 动态绑定路由(把拿到的menuList循环)
            let newRoutes = router.options.routes
            res.data.data.nav.forEach(menu => {
                if (menu.children) {
                    menu.children.forEach(e => {
                        // 转换成路由
                        if (e.component) {
                            console.log("e.component", e)
                            let route = {
                                name: e.name,
                                path: e.path,
                                mate: {
                                    icon: e.icon,
                                    title: e.title
                                },
                                title:e.title,
                                icon:e.icon,
                                component: () => import("@/views/" + e.component + '.vue')
                            }
                            // 把转换后的路由添加到路由管理器
                            if (route) {
                                newRoutes[0].children.push(route)
                            }
                        }

                    })
                }
            })
            console.log("newRoutes", newRoutes)
            router.addRoutes(newRoutes)
            hasRoute = true
            console.log("hasRoute2", hasRoute)
            store.commit("changeRouteStatus", hasRoute)

        })
    }

    next()
}))

export default router

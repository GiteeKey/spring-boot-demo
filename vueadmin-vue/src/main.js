import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Element from 'element-ui'
import "element-ui/lib/theme-chalk/index.css"

// import axios from 'axios'
import axios from "@/axios";

Vue.prototype.$axios = axios  //全局使用
Vue.config.productionTip = false

Vue.use(Element)
require("./mock.js")
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')

Vue.mixin({
    methods: {
        hasAuth(perm) {
            var authority = this.$store.state.menus.permList
            console.log(authority)
            return authority.indexOf(perm) > -1
        }
    }
})

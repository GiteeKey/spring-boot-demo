import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default {
    state: {
        // 导航栏菜单
        menuList: [],
        // 权限
        permList: [],
        // 每次刷新都获取菜单，消耗服务器，可以将他存到sessionStorage中
        hasRoute: false,
        // tab标签默认为首页
        editableTabsValue: 'Index',
        editableTabs: [{
            title: '首页',
            name: 'Index',
        }],
    },
    mutations: {
        setMenuList(state, menuList) {
            state.menuList = menuList
        },
        setPermList(state, permList) {
            state.setPermList = permList;
        },
        changeRouteStatus(state, hasRoute) {
            state.hasRoute = hasRoute;
        },

        // 添加tab标签
        addTab(state, tab) {
            let index = state.editableTabs.findIndex(e => e.name === tab.name);
            if (index === -1) {
                state.editableTabs.push({
                    title: tab.title,
                    name: tab.name,
                });
            }
            this.editableTabsValue = tab;
        },

        resetState: (state) => {
            // 导航栏菜单
            state.menuList = [],
                // 权限
                state.permList = [],
                // 每次刷新都获取菜单，消耗服务器，可以将他存到sessionStorage中
                state.hasRoute = false,
                // tab标签默认为首页
                state.editableTabsValue = 'Index',
                state.editableTabs = [{
                    title: '首页',
                    name: 'Index',
                }]
        },
    },
    actions: {}
    ,
    modules: {}
}

# vueadmin-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# 说明

```tex
VueAdmin - 前后端分离后台管理系统

线上演示：https://www.markerhub.com/vueadmin
登录密码：1234567

前端笔记：https://shimo.im/docs/pxwyJHgqcWjWkTKX/

后端笔记：https://shimo.im/docs/OnZDwoxFFL8bnP1c/

源码分享：
https://github.com/markerhub/vueadmin
https://gitee.com/markerhub/VueAdmin

视频讲解：https://www.bilibili.com/video/BV1af4y1s7Wh/
```




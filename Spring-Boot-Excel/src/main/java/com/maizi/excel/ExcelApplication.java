package com.maizi.excel;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class ExcelApplication {
    public static void main(String[] args) {
        SpringApplication.run(ExcelApplication.class, args);
        log.info("ExcelWrite是使用EasyExcel写");
        log.info("ExcelRead是使用EasyExcel读，ExcelListenter是监听器");
        log.info("ReadAndSave是使用EasyExcel读取数据并保存");
        log.info("模板打印Demo==》  http://localhost:8080//printExcel?inputDate=2021-10-11");
    }
}

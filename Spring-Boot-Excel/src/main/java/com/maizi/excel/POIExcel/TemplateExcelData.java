package com.maizi.excel.POIExcel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TemplateExcelData {

    private String UUID;
    private String name;
    private String age;
    private String nickName;
    private String ip;
    private String birthday;
    private String card;
    private String remarks;

    // setter getter ……
}

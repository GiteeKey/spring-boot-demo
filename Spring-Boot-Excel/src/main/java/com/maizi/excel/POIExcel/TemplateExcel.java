package com.maizi.excel.POIExcel;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class TemplateExcel {

    @Autowired
    private HttpServletResponse response;


    //模板下载
    @GetMapping("/printExcel")
    public void printExcel(String inputDate) throws IOException {
        //先调用service查询要打印的内容,这里先模拟数据
        List<TemplateExcelData> list = new ArrayList<>();
        list.add(new TemplateExcelData("20211102","王五","18","易烊千玺说我是他的小姑娘","192.168.0.123","2021-10-15","513436200011028752","千玺年的诗"));
        list.add(new TemplateExcelData("20211103","赵毛豆","25","会飞的猪","192.168.0.158","2021-11-15","513436200011026255","备注"));
        list.add(new TemplateExcelData("20211104","闻人萱","96","海豹突击队队长的烊烊","192.168.35.196","2021-12-15","513436200011027936","时倾_贰捌"));
        list.add(new TemplateExcelData("20211105","徐离伟","13","黑不溜秋的二十耶","10.168.0.123","2021-06-15","513436200011026837","烊烊不熬夜"));
        list.add(new TemplateExcelData("20211106","邹忠","52","我跟所有人都不熟","20.168.0.255","2021-05-23","513436200011029210","备注哦"));
        list.add(new TemplateExcelData("20211107","巩娇","06","易流年不复返","192.896.0.258","2021-01-23","513436200011027178","祁澜致，发给"));
        System.out.println("数据长度"+list.size());
        //先读取模板
        //1.获取模板的路径
        // String path = session.getServletContext().getRealPath("/") + "/xlsprint/tOUTPRODUCT.xlsx";
        String path = this.getClass().getClassLoader().getResource("xlsprint/tOUTPRODUCT.xlsx").getPath();
        //2.创建Workbook对象
        Workbook workbook = new XSSFWorkbook(path);
        //3.获取页
        Sheet sheetAt = workbook.getSheetAt(0);
        //4.处理大标题
        //获取标题行
        Row row = sheetAt.getRow(0);
        //获取标题单元格
        Cell cell = row.getCell(1);
        //设置标题内容
        inputDate = inputDate.replaceAll("-0", "-").replaceAll("-", "年");
        // 设置文件名称
        cell.setCellValue(inputDate + "用户表");
        //5.处理小标题行内容（小标题不改变，所以不用处理）
        //6.提取数据列的样式
        row = sheetAt.getRow(2);//获取数据的行
        CellStyle css[] = new CellStyle[9];//有8个单元格
        for (int i = 1; i < css.length; i++) {
            cell = row.getCell(i);  //获取单元格
            css[i] = cell.getCellStyle();//获取样式
        }
        //7.填充数据
        int index = 2;
        for (TemplateExcelData productVo : list) {
            row = sheetAt.createRow(index);//创建第3行，索引为2
            cell = row.createCell(1); //创建单元格
            cell.setCellValue(productVo.getUUID()); //设置内容
            cell.setCellStyle(css[1]); //样式
            /*一次类推*/
            cell = row.createCell(2);
            cell.setCellValue(productVo.getName());
            cell.setCellStyle(css[2]);
            cell = row.createCell(3);
            cell.setCellValue(productVo.getAge());
            cell.setCellStyle(css[3]);
            cell = row.createCell(4);
            cell.setCellValue(productVo.getNickName());
            cell.setCellStyle(css[4]);
            cell = row.createCell(5);
            cell.setCellValue(productVo.getIp());
            cell.setCellStyle(css[5]);
            cell = row.createCell(6);
            cell.setCellValue(productVo.getBirthday());
            cell.setCellStyle(css[6]);
            cell = row.createCell(7);
            cell.setCellValue(productVo.getCard());
            cell.setCellStyle(css[7]);
            cell = row.createCell(8);
            cell.setCellValue(productVo.getRemarks());
            cell.setCellStyle(css[8]);
            index++;
        }

        /**
         * 下载
         * 三个参数 ： ByteArrayOutputStream byteArrayOutputStream,
         * HttpServletResponse response,
         * String returnName
         */
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        workbook.write(outputStream);
        new DownloadUtil().download(outputStream, response, inputDate + "月用户表.xlsx");
    }
}

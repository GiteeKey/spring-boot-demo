package com.maizi.excel.EasyExcel.ExcelReadAndWriteDemo.test;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class ExamInfo {

    private String answer;

    private String stem;

    private String analyse;

    private Integer singleScore;

    private List<EaxmChoiceInfo> eaxmChoiceInfoList=new ArrayList<>();
// 省略get/set
}

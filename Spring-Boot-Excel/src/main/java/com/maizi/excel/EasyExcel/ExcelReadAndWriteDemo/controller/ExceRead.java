package com.maizi.excel.EasyExcel.ExcelReadAndWriteDemo.controller;

import com.alibaba.excel.EasyExcel;
import com.maizi.excel.EasyExcel.ExcelReadAndWriteDemo.domain.ExcelData;
import com.maizi.excel.EasyExcel.ExcelReadAndWriteDemo.listener.ExcelListenter;

public class ExceRead {

    public static void main(String[] args) {

        // 读取excel的位置
        String fileName = "/Users/abraham/Desktop/内容填写确认表样.xlsx";

        /**
         *  fileName   读取excel的位置
         *  ExcelData.class   实体类
         *  new ExcelListenter()   监听器
         */
        EasyExcel.read(fileName, ExcelData.class, new ExcelListenter()).sheet().doRead();
    }


}

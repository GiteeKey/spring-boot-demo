package com.maizi.excel.EasyExcel.ExcelReadAndWriteDemo.test;

import com.alibaba.excel.EasyExcel;

import java.util.ArrayList;
import java.util.List;

public class PersonalExcel {

    public static void main(String[] args) {
        List<ExamInfo> examInfoList = new ArrayList<>();
        String fileName = "/Users/abraham/Desktop/内容填写确认表样.xlsx";

        EasyExcel.read(fileName, ExamMsg.class, new ExamListener(examInfos -> {
            for (ExamInfo examInfo : examInfos) {
                examInfoList.add(examInfo);
            }
        })).sheet().doRead();
        System.out.println(examInfoList);
    }
}

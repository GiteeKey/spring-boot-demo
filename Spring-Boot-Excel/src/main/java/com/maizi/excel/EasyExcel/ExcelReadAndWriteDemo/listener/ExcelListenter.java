package com.maizi.excel.EasyExcel.ExcelReadAndWriteDemo.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.maizi.excel.EasyExcel.ExcelReadAndWriteDemo.domain.ExcelData;


import java.util.Map;

public class ExcelListenter extends AnalysisEventListener<ExcelData> {
    /**
     * 读取表头
     *
     * @param headMap  表头
     * @param context
     */
    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("表头内容 = " + headMap);
    }

    /**
     * 一行一行的读取
     *
     * @param readData        实体类
     * @param analysisContext
     */
    @Override
    public void invoke(ExcelData readData, AnalysisContext analysisContext) {
        System.out.println("readData = " + readData);
    }

    /**
     * 读取完成后要做的事
     *
     * @param analysisContext
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }

}

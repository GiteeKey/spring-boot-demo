package com.maizi.excel.EasyExcel.controller;

import org.springframework.stereotype.Service;

@Service
public class SubjectService {

    /**
     * 模拟调用保存方法
     *
     * @param subjectData
     */
    public void save(SubjectData subjectData) {
        System.out.println("调用了保存方法 " + subjectData);
    }
}

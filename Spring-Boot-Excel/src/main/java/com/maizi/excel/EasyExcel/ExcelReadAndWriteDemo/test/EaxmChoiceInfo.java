package com.maizi.excel.EasyExcel.ExcelReadAndWriteDemo.test;

import lombok.Data;

@Data
public class EaxmChoiceInfo {

    private String choiceType;

    private String optionContent;

    public EaxmChoiceInfo(String choiceType, String optionContent) {
        this.choiceType = choiceType;
        this.optionContent = optionContent;
    }

    // 省略get/set
}

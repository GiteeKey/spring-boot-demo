package com.maizi.excel.EasyExcel.ExcelReadAndWriteDemo.controller;

import com.alibaba.excel.EasyExcel;
import com.maizi.excel.EasyExcel.ExcelReadAndWriteDemo.domain.ExcelData;

import java.util.ArrayList;
import java.util.List;

public class ExcelWrite {

    public static void main(String[] args) {

        // 文件的下载路径(或写入路径)
        String fileName = "E:\\Excel写学习Demo.xlsx";

        /**
         * .write(fileName, ExcelData.class)   fileName 是写入路径， ExcelData.class 是接收实体类
         * .sheet 是sheet名称
         * .doWrite 是要写入的数据List
         *
         */
        EasyExcel.write(fileName, ExcelData.class).sheet("写Demo测试").doWrite(getData());
    }

    /**
     * 模拟写入的数据
     *
     * @return
     */
    private static List getData() {
        ArrayList<ExcelData> excelDataList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ExcelData excelData = new ExcelData();
            excelData.setData1("测试第一列" + i);
            excelData.setData2("测试第二列" + i);
            excelDataList.add(excelData);
        }

        return excelDataList;
    }
}

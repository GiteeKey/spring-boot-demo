package com.maizi.excel.EasyExcel.ExcelReadAndWriteDemo.domain;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;

@Data
public class ExcelData {

    /**
     * @ExcelProperty
     * value = "第一列" 设置Excel表头名称
     * index = 0 表示第一列
     *
     *  @ColumnWidth  设置宽度
     */
    @ExcelProperty(value = "第一列", index = 0)
    @ColumnWidth(100)
    private String data1;
    @ExcelProperty(value = "第二列", index = 1)
    @ColumnWidth(100)
    private String data2;
}

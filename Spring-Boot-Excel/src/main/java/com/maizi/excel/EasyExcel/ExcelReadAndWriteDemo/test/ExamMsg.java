package com.maizi.excel.EasyExcel.ExcelReadAndWriteDemo.test;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class ExamMsg {

    @ExcelProperty("题目描述")
    private String answer;

    @ExcelProperty("考试答案")
    private String stem;

    @ExcelProperty("考试答案分析")
    private String analyse;

    @ExcelProperty("题目单项分数")
    private Integer singleScore;


    @ExcelProperty("选项")
    private String choiceType;

    @ExcelProperty("选项内容")
    private String optionContent;
	
	// 省略get/set
}

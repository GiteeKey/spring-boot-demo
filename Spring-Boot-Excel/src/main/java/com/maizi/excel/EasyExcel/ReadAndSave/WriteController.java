package com.maizi.excel.EasyExcel.controller;

import com.alibaba.excel.EasyExcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Controller
public class WriteController {

    @Autowired
    private SubjectService subjectService;

    @PostMapping("/upload/excel")
    public String writeAddSave(MultipartFile excelFile) {

        try {
            // 获取文件流
            InputStream inputStream = excelFile.getInputStream();
            /**
             *  inputStream   文件流
             *  SubjectData.class   实体类
             *  new SubjectExcelListener(subjectService)   监听器(主要功能都在监听器里，比如读取添加等操作)
             *  SubjectExcelListener  没有交给Spring进行管理，所有不能在此方法使用    @Autowired 注入，所以使用  SubjectExcelListener(subjectService)   传入方式
             */
            EasyExcel.read(inputStream, SubjectData.class, new SubjectExcelListener(subjectService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
            return "保存失败";
        }
        return "保存成功";

    }

}

package com.maizi.excel.EasyExcel.ExcelReadAndWriteDemo.test;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.read.listener.ReadListener;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class ExamListener implements ReadListener<ExamMsg> {

  // 自定义消费者函数接口用于自定义监听器中数据组装
    private final Consumer<List<ExamInfo>> consumer;

    public ExamListener(Consumer<List<ExamInfo>> consumer) {
        this.consumer = consumer;
    }

    // easy excel读取参数
    private List<ExamMsg> examMsgList=new ArrayList<>();
    // 封装读取对象
    private List<ExamInfo> examInfoList=new ArrayList<>();

    @Override
    public void onException(Exception e, AnalysisContext analysisContext) throws Exception {

    }

    @Override
    public void invokeHead(Map<Integer, CellData> map, AnalysisContext analysisContext) {

    }

    // 每行读取完成之后会执行
    @Override
    public void invoke(ExamMsg data, AnalysisContext context)  {
        // 按照格式组装数据
        if(data.getStem() != null){
            ExamInfo examInfo = new ExamInfo();
            BeanUtils.copyProperties(data,examInfo);
            examInfo.getEaxmChoiceInfoList().add(new EaxmChoiceInfo(data.getChoiceType(),data.getOptionContent()));
            examInfoList.add(examInfo);
        }else {
            // 倒序添加选择题信息,只对最后一个进行添加选项数据信息
            examInfoList.get(examInfoList.size() - 1).getEaxmChoiceInfoList().add(new EaxmChoiceInfo(data.getChoiceType(),data.getOptionContent()));
        }

    }
	// 每行读取完成之后执行
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
         if (CollectionUtils.isNotEmpty(examInfoList)) {
            consumer.accept(examInfoList);
        }
    }

    @Override
    public boolean hasNext(AnalysisContext analysisContext) {
        return false;
    }
}

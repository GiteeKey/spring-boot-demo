package com.maizi.generator.service.impl;

import com.maizi.generator.domain.MLounge;
import com.maizi.generator.mapper.MLoungeMapper;
import com.maizi.generator.service.IMLoungeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 休息室 服务实现类
 * </p>
 *
 * @author 一粒麦子
 * @since 2024-03-29
 */
@Service
public class MLoungeServiceImpl extends ServiceImpl<MLoungeMapper, MLounge> implements IMLoungeService {

}

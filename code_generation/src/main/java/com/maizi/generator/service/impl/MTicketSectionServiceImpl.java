package com.maizi.generator.service.impl;

import com.maizi.generator.domain.MTicketSection;
import com.maizi.generator.mapper.MTicketSectionMapper;
import com.maizi.generator.service.IMTicketSectionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 票的航段 服务实现类
 * </p>
 *
 * @author 一粒麦子
 * @since 2024-03-29
 */
@Service
public class MTicketSectionServiceImpl extends ServiceImpl<MTicketSectionMapper, MTicketSection> implements IMTicketSectionService {

}

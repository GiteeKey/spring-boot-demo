package com.maizi.generator.service;

import com.maizi.generator.domain.MTicketSection;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 票的航段 服务类
 * </p>
 *
 * @author 一粒麦子
 * @since 2024-03-29
 */
public interface IMTicketSectionService extends IService<MTicketSection> {

}

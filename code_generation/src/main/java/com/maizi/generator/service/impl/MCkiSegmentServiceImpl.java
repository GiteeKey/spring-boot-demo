package com.maizi.generator.service.impl;

import com.maizi.generator.domain.MCkiSegment;
import com.maizi.generator.mapper.MCkiSegmentMapper;
import com.maizi.generator.service.IMCkiSegmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 一粒麦子
 * @since 2024-03-29
 */
@Service
public class MCkiSegmentServiceImpl extends ServiceImpl<MCkiSegmentMapper, MCkiSegment> implements IMCkiSegmentService {

}

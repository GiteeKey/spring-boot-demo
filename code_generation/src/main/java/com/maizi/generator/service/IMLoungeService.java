package com.maizi.generator.service;

import com.maizi.generator.domain.MLounge;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 休息室 服务类
 * </p>
 *
 * @author 一粒麦子
 * @since 2024-03-29
 */
public interface IMLoungeService extends IService<MLounge> {

}

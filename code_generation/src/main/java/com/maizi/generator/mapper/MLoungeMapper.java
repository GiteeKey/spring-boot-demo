package com.maizi.generator.mapper;

import com.maizi.generator.domain.MLounge;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 休息室 Mapper 接口
 * </p>
 *
 * @author 一粒麦子
 * @since 2024-03-29
 */
public interface MLoungeMapper extends BaseMapper<MLounge> {

}

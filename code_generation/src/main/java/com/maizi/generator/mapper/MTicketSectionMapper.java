package com.maizi.generator.mapper;

import com.maizi.generator.domain.MTicketSection;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 票的航段 Mapper 接口
 * </p>
 *
 * @author 一粒麦子
 * @since 2024-03-29
 */
public interface MTicketSectionMapper extends BaseMapper<MTicketSection> {

}

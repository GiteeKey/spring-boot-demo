package com.maizi.generator.mapper;

import com.maizi.generator.domain.MCkiSegment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 一粒麦子
 * @since 2024-03-29
 */
public interface MCkiSegmentMapper extends BaseMapper<MCkiSegment> {

}

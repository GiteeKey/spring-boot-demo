package com.maizi.generator.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 一粒麦子
 * @since 2024-03-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("M_CKI_SEGMENT")
public class MCkiSegment implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.ASSIGN_ID)
    private BigDecimal id;

    @TableField("CKI_ID")
    private BigDecimal ckiId;

    @TableField("TRAVELLER_ID")
    private BigDecimal travellerId;

    @TableField("CANCELED")
    private String canceled;

    @TableField("HAS_INFANT")
    private String hasInfant;

    @TableField("IO_FLAG")
    private String ioFlag;

    @TableField("STATUS")
    private String status;

    @TableField("SEAT_NUM")
    private String seatNum;

    @TableField("TICKET_NUMBER")
    private String ticketNumber;

    @TableField("COUPON_NUM")
    private String couponNum;

    @TableField("DEP_AIRPORT_CODE")
    private String depAirportCode;

    @TableField("DEP_DATE")
    private String depDate;

    @TableField("DEP_TIME")
    private String depTime;

    @TableField("DEP_DATETIME")
    private Date depDatetime;

    @TableField("ARR_AIRPORT_CODE")
    private String arrAirportCode;

    @TableField("ARR_DATE")
    private String arrDate;

    @TableField("ARR_TIME")
    private String arrTime;

    @TableField("ARR_DATETIME")
    private Date arrDatetime;

    @TableField("CARRIER_AIRLINE_CODE")
    private String carrierAirlineCode;

    @TableField("CARRIER_FLIGHT_NUMBER")
    private String carrierFlightNumber;

    @TableField("CARRIER_SUFFIX")
    private String carrierSuffix;

    @TableField("CARRIER_CABIN_CLASS")
    private String carrierCabinClass;

    @TableField("MC_AIRLINE_CODE")
    private String mcAirlineCode;

    @TableField("MC_FLIGHT_NUMBER")
    private String mcFlightNumber;

    @TableField("MC_SUFFIX")
    private String mcSuffix;

    @TableField("MC_CABIN_CLASS")
    private String mcCabinClass;

    @TableField("HAS_BAG")
    private String hasBag;

    @TableField("BAG_COUNT")
    private Integer bagCount;

    @TableField("TOTAL_WEIGHT")
    private BigDecimal totalWeight;

    @TableField("FOOD")
    private String food;

    @TableField("BLIND")
    private String blind;

    @TableField("DEAF")
    private String deaf;

    @TableField("WHEEL_CHAIR")
    private String wheelChair;

    @TableField("STRETCHER")
    private String stretcher;

    @TableField("WTH_INFN")
    private String wthInfn;

    @TableField("UNACCOMPANIED")
    private String unaccompanied;

    @TableField("CHILD")
    private String child;

    @TableField("SSR_CODE")
    private String ssrCode;

    @TableField("SSR_TEXT")
    private String ssrText;

    @TableField("SPECIAL_FLAG")
    private String specialFlag;

    @TableField("PAY_SEAT_FLAG")
    private String paySeatFlag;

    @TableField("BAG_NO")
    private String bagNo;

    @TableField("SEAT_TYPE")
    private String seatType;

    @TableField("REGULAR_NUMBER")
    private String regularNumber;

    @TableField("REGULAR_AIRLINE")
    private String regularAirline;

    @TableField("REGULAR_STATUS")
    private String regularStatus;

    /**
     * 值机方式 0：其他值机 1：网上值机 2：CUSS值机 3：城市值机 4：码头值机
     */
    @TableField("CHECK_IN_MODE")
    private Integer checkInMode;

    /**
     * 值机序号
     */
    @TableField("BOARDING_NUM")
    private String boardingNum;

    /**
     * YYYYMMDD	值机日期
     */
    @TableField("CHECK_IN_DATE")
    private String checkInDate;

    /**
     * hhmm	值机时间
     */
    @TableField("CHECK_IN_TIME")
    private String checkInTime;

    /**
     * checkin 文本信息
     */
    @TableField("CKIN")
    private String ckin;

    /**
     * 候补标识（YN）
     */
    @TableField("STOODBY")
    private String stoodby;

    /**
     * 候补号
     */
    @TableField("STANDBY_NUM")
    private String standbyNum;

    /**
     * 由候补接收标识（YN）
     */
    @TableField("ACC_STANDBY")
    private String accStandby;

    /**
     * 未订座标识（YN）
     */
    @TableField("GOSHOW")
    private String goshow;

    /**
     * 两舱标识（YN）
     */
    @TableField("FC_IND")
    private String fcInd;

    /**
     * 行李类型（区分其他行李，如AVIH等，目前只有BGE一种类型）
     */
    @TableField("BAG_TYPE")
    private String bagType;


}

package com.maizi.generator.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 休息室
 * </p>
 *
 * @author 一粒麦子
 * @since 2024-03-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("M_LOUNGE")
public class MLounge implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "ID", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 航班日期
     */
    @TableField("FLT_DATE")
    private String fltDate;

    /**
     * 序号
     */
    @TableField("FLT_SEQ")
    private String fltSeq;

    /**
     * 准入时间
     */
    @TableField("IN_TIME")
    private String inTime;

    /**
     * 休息室编号
     */
    @TableField("LOUNGE_CODE")
    private String loungeCode;

    /**
     * 休息室名称
     */
    @TableField("LOUNGE_NAME")
    private String loungeName;

    /**
     * 休息室官方名称
     */
    @TableField("OFFICIAL_NAME")
    private String officialName;

    /**
     * 旅客状态  0：进入 , 1：离开
     */
    @TableField("STATUS")
    private String status;

    /**
     * 起始地
     */
    @TableField("ORIG")
    private String orig;

    /**
     * 目的地
     */
    @TableField("DEST")
    private String dest;

}

package com.maizi.generator.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 票的航段
 * </p>
 *
 * @author 一粒麦子
 * @since 2024-03-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("M_TICKET_SECTION")
public class MTicketSection implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.ASSIGN_ID)
    private String id;

    @TableField("ETS_ID")
    private BigDecimal etsId;

    /**
     * 旅客ID
     */
    @TableField("TRAVELLER_ID")
    private BigDecimal travellerId;

    /**
     * 票号
     */
    @TableField("TICKET_NUMBER")
    private String ticketNumber;

    /**
     * 航段号
     */
    @TableField("COUPON_NUM")
    private String couponNum;

    /**
     * 票段状态,A/C/E/F/G/I
     */
    @TableField("COUPON_STATUS")
    private String couponStatus;

    /**
     * 航段状态 OK：座位确认 RQ：未确认或候补（请求状态） SA：该座位可用 NS：婴儿，不占座
     */
    @TableField("SEGMENT_STATUS")
    private String segmentStatus;

    /**
     * PNR编号，SourceRef
     */
    @TableField("ICS_RECORD_LOCATOR")
    private String icsRecordLocator;

    /**
     * CRS PNR编号，VendorLocator
     */
    @TableField("CRS_RECORD_LOCATOR")
    private String crsRecordLocator;

    /**
     * 特殊标识，目前只有IRR
     */
    @TableField("SPECIAL_FLAG")
    private String specialFlag;

    /**
     * 航信提供航班状态标识
     */
    @TableField("SCHEDULE_CHANGE")
    private String scheduleChange;

    /**
     * 该航段有效期的起始日期
     */
    @TableField("NOT_VALID_BEFORE")
    private String notValidBefore;

    /**
     * 该航段有效期的终止日期
     */
    @TableField("NOT_VALID_AFTER")
    private String notValidAfter;

    /**
     * 航段属性:FLT,ARNK,OPEN,空
     */
    @TableField("COUPON_ATTR")
    private String couponAttr;

    /**
     * 起飞机场名称
     */
    @TableField("DEP_AIRPORT_NAME")
    private String depAirportName;

    /**
     * 起飞机场编码
     */
    @TableField("DEP_AIRPORT_CODE")
    private String depAirportCode;

    /**
     * 起飞日期
     */
    @TableField("DEP_DATE")
    private String depDate;

    /**
     * 起飞时间
     */
    @TableField("DEP_TIME")
    private String depTime;

    /**
     * 起飞航站楼
     */
    @TableField("DEP_TERMINAL")
    private String depTerminal;

    /**
     * 到达机场名称
     */
    @TableField("ARR_AIRPORT_NAME")
    private String arrAirportName;

    /**
     * 到达机场编码
     */
    @TableField("ARR_AIRPORT_CODE")
    private String arrAirportCode;

    /**
     * 到达日期
     */
    @TableField("ARR_DATE")
    private String arrDate;

    /**
     * 到达时间
     */
    @TableField("ARR_TIME")
    private String arrTime;

    /**
     * 到达航站楼
     */
    @TableField("ARR_TERMINAL")
    private String arrTerminal;

    @TableField("DEP_DATETIME")
    private Date depDatetime;

    @TableField("ARR_DATETIME")
    private Date arrDatetime;

    /**
     * 承运航空公司代码
     */
    @TableField("CARRIER_AIRLINE_CODE")
    private String carrierAirlineCode;

    /**
     * 承运航班号
     */
    @TableField("CARRIER_FLIGHT_NUMBER")
    private String carrierFlightNumber;

    /**
     * 承运航班代码后缀
     */
    @TableField("CARRIER_SUFFIX")
    private String carrierSuffix;

    /**
     * 市场航空公司代码
     */
    @TableField("MC_AIRLINE_CODE")
    private String mcAirlineCode;

    /**
     * 市场航班号
     */
    @TableField("MC_FLIGHT_NUMBER")
    private String mcFlightNumber;

    /**
     * 市场航班代码后缀
     */
    @TableField("MC_SUFFIX")
    private String mcSuffix;

    /**
     * 成行舱位
     */
    @TableField("CLASS_OF_SERVICE")
    private String classOfService;

    /**
     * 起始城市编码
     */
    @TableField("PREVIOUS_ARRIVAL")
    private String previousArrival;

    /**
     * 到达城市编码
     */
    @TableField("NEXT_DEPARTURE")
    private String nextDeparture;

    /**
     * ICS系统PNR记录编号
     */
    @TableField("SOURCE_REF")
    private String sourceRef;

    /**
     * Open航段的起飞机场
     */
    @TableField("OPEN_DEPARTURE")
    private String openDeparture;

    /**
     * Open航段的到达机场
     */
    @TableField("OPEN_ARRIVAL")
    private String openArrival;

    /**
     * 承运方信息
     */
    @TableField("CARRIER_AIRLINE_INFO")
    private String carrierAirlineInfo;

    /**
     * 市场方信息
     */
    @TableField("MARKET_AIRLINE_CODE")
    private String marketAirlineCode;

    /**
     * OPEN时成行舱位
     */
    @TableField("OPEN_CLASS_OF_SERVICE")
    private String openClassOfService;

    /**
     * 中转标识
     */
    @TableField("IO_FLAG")
    private String ioFlag;

    /**
     * 是否携带婴儿(Y、N)
     */
    @TableField("HAS_INFANT")
    private String hasInfant;

    /**
     * 值机状态（AC、NA）
     */
    @TableField("STATUS")
    private String status;

    /**
     * 值机方式:0：其他值机,1：网上值机,2：CUSS值机,3：城市值机,4：码头值机
     */
    @TableField("CHECK_IN_MODE")
    private String checkInMode;

    /**
     * 值机序号
     */
    @TableField("BOARDING_NUM")
    private String boardingNum;

    /**
     * 取消标识（Y/N）
     */
    @TableField("CANCELED")
    private String canceled;

    /**
     * 值机日期
     */
    @TableField("CHECK_IN_DATE")
    private String checkInDate;

    /**
     * 值机时间
     */
    @TableField("CHECK_IN_TIME")
    private String checkInTime;

    /**
     * checkin 文本信息
     */
    @TableField("CKIN")
    private String ckin;

    /**
     * 候补标识（Y/N）
     */
    @TableField("STOODBY")
    private String stoodby;

    /**
     * 候补号
     */
    @TableField("STANDBY_NUM")
    private String standbyNum;

    /**
     * 由候补接收标识（Y/N）
     */
    @TableField("ACC4_STANDBY")
    private String acc4Standby;

    /**
     * 未订座标识（Y/N）
     */
    @TableField("GOSHOW")
    private String goshow;

    /**
     * 座位号
     */
    @TableField("SEAT_NUM")
    private String seatNum;

    /**
     * 座位类型
     */
    @TableField("SEAT_TYPE")
    private String seatType;

    /**
     * 两舱标识（Y/N）
     */
    @TableField("FC_IND")
    private String fcInd;

    /**
     * 是否有行李（Y、N）
     */
    @TableField("HAS_BAG")
    private String hasBag;

    /**
     * 行李类型（区分其他行李，如AVIH等，目前只有BGE一种类型）
     */
    @TableField("BAG_TYPE")
    private String bagType;

    /**
     * 行李个数
     */
    @TableField("BAG_COUNT")
    private BigDecimal bagCount;

    /**
     * 行李总重量
     */
    @TableField("TOTAL_WEIGHT")
    private BigDecimal totalWeight;

    /**
     * 免费行李重量
     */
    @TableField("FREE_BAG_WEIGHT")
    private BigDecimal freeBagWeight;

    /**
     * 免费行李个数
     */
    @TableField("FREE_BAG_COUNT")
    private BigDecimal freeBagCount;

    /**
     * 旅客当前出行状态
     */
    @TableField("CURRENT_STATUS")
    private String currentStatus;

    /**
     * 旅客进入休息室状态
     */
    @TableField("REST_STATUS")
    private String restStatus;

    /**
     * 客票创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 离港初始化时间
     */
    @TableField("ETD_TIME")
    private Date etdTime;

    /**
     * 首次产生行李号（行李办理）
     */
    @TableField("FIRST_BAG_TIME")
    private Date firstBagTime;

    /**
     * 登机时间
     */
    @TableField("BOARD_TIME")
    private Date boardTime;

    /**
     * 进入休息室时间
     */
    @TableField("ENTER_REST_TIME")
    private Date enterRestTime;

    /**
     * 安检时间
     */
    @TableField("SECURITY_CHECK_TIME")
    private Date securityCheckTime;

    /**
     * 最新更新时间
     */
    @TableField("LAST_UPDATE_TIME")
    private Date lastUpdateTime;

    /**
     * 值机日期时间
     */
    @TableField("CHECK_IN_DATETIME")
    private Date checkInDatetime;

    /**
     * 行李号码,多个使用｜分割
     */
    @TableField("BAG_NO")
    private String bagNo;

    /**
     * 登机状态
     */
    @TableField("BOARD_STATUS")
    private String boardStatus;

    /**
     * 是否已经进行出行提醒
     */
    @TableField("TRAVEL_TIPS")
    private String travelTips;


}

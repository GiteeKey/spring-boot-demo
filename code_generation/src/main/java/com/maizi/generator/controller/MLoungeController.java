package com.maizi.generator.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 休息室 前端控制器
 * </p>
 *
 * @author 一粒麦子
 * @since 2024-03-29
 */
@RestController
@RequestMapping("/m-lounge")
public class MLoungeController {

}

package com.maizi.websorcket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebSorcketApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebSorcketApplication.class, args);
        System.out.println("测试   打开网址 http://www.ecjson.com/websocket  \n" +
                "网页一输入  输入连接地址 ws://127.0.0.1:9400/webSocket/1 \n" +
                "网页二输入  输入连接地址 ws://127.0.0.1:9400/webSocket/2 \n" +
                "在网页二输入 发送以下消息测试网页一是否收到 \n" +
                "{\"to\":\"1\",\"source\":\"2\",\"type\":\"消息类型\",\"msg\":\"消息内容\"}");
    }
}

package com.maizi.websorcket.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.Map;

/**
 * @author abraham
 */
@Component
public class MessageInterceptor implements HandshakeInterceptor {

    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler webSocketHandler, Map<String, Object> map) throws Exception {
        // path = "/webSocket/{userId}"   是 WebSocketConfig 中配置的路径
        String path = request.getURI().getPath();
        if (StringUtils.isEmpty(path)) {
            return false;
        }
        // [webSocket,{userId}]
        String[] split = path.split("/");
        if (null == split) {
            return false;
        }
        // 将 userId 传递到 MessageHandler
        map.put("userId", split[2]);
        return true;
    }

    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler webSocketHandler, Exception e) {

    }
}

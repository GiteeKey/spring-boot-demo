package com.maizi.websorcket.config;

import com.alibaba.fastjson2.JSON;
import com.maizi.websorcket.domain.Message;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author abraham
 */
@Log4j2
@Component
public class MessageHandler extends TextWebSocketHandler {

    public static final Map<String, WebSocketSession> SESSION_MAP = new ConcurrentHashMap<>();

    /**
     * 刚进入websocket
     *
     * @param session
     * @throws Exception
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        // 获取从MessageInterceptor 传过来的 userId
        String userId = session.getAttributes().get("userId").toString();
        SESSION_MAP.put(userId, session);
//        this.handleTextMessage(session, new TextMessage("123"));
    }

    /**
     * 发送消息
     *
     * @param session
     * @param message
     * @throws Exception
     */
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        // 获取接受到的消息 message.getPayload()
        log.info("接收到的消息 {} ", message.getPayload());
        // 发送消息  // session.sendMessage(new TextMessage("服务器返回信息，连接成功"));

        // 提示： message 可以为  {"to":"发送给谁","source":"谁发送","type":"消息类型","msg":"消息内容"} ，然后
        String payload = message.getPayload();
        Message msg = JSON.parseObject(payload, Message.class);
        String to = msg.getTo(); // 发送给谁
        WebSocketSession webSocketSession = SESSION_MAP.get(to);
        if (null != webSocketSession){
            // 发送消息
            webSocketSession.sendMessage(new TextMessage(msg.getMsg()));
        }
    }

    /**
     * 关闭websocket
     *
     * @param session
     * @param status
     * @throws Exception
     */
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {

    }
}

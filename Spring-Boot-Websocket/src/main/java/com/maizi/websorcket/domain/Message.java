package com.maizi.websorcket.domain;

import lombok.Data;
import lombok.ToString;

/**
 * @author ham
 */
@Data
@ToString
public class Message {

    /* 发送给谁¬ */
    private String to;

    /* 消息来源 */
    private String source;

    /* 消息类型 */
    private String type;

    /* 消息内容 */
    private String msg;
}

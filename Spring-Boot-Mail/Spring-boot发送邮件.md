## 0.学习来源

```tex
https://gitlab.coding-space.cn/demo/email
```

## 1.163邮箱

**POP3/SMTP服务并记住授权码**

![image-20210905094445423](https://gitee.com/GiteeKey/mblog/raw/master/images/image-20210905094445423.png)

## 2.依赖

```xml
<!--    SpringBoot父依赖-->
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.1.6.RELEASE</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>


<dependencies>
    <!--       web依赖-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <!--springBoot测试依赖-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>
    <!--     Mail依赖-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-mail</artifactId>
    </dependency>
</dependencies>
```

## 3.application配置

```properties
# 端口号
server.port=9100
# 163的mail主机
spring.mail.host=smtp.163.com
# 发送邮箱，改成你自己的
spring.mail.username=换成你的发送邮箱
# 发送邮箱的授权码
spring.mail.password=换成你的授权码
# 端口号
spring.mail.port=25
# 文件最大100MB
spring.servlet.multipart.max-file-size=100MB
spring.servlet.multipart.max-request-size=100MB
```

## 4.发送邮件

```java
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

@RestController
@RequestMapping("/sendMail")
public class SendMailController {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String account;


    @PostMapping("/simpleMail")
    @ApiOperation("发送简单邮件")
    public String sendSimpleMail() {
        SimpleMailMessage smm = new SimpleMailMessage();
        // 谁发送
        smm.setFrom(account);
        // 谁接收
        smm.setTo(new String[]{"zhaoyi_java@163.com"});
        smm.setSubject("测试发送简单邮件");
        smm.setText("文本内容");
        javaMailSender.send(smm);
        return "简答邮件发送成功";
    }

    /**
     * 发送附件邮件
     */
    @PostMapping("/attachmentMail")
    @ApiOperation("发送带附件的邮件")
    public String sendAttachmentMail(
            @ApiParam("收件邮箱") @RequestParam String address,
            @ApiParam("标题") @RequestParam String subject,
            @ApiParam("正文") @RequestParam String body,
            @ApiParam("附件") @RequestPart MultipartFile file) throws MessagingException, IOException {
        MimeMessage mimeMailMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMailMessage, true);
        // 谁发送
        mimeMessageHelper.setFrom(account);
        // 发送给谁
        mimeMessageHelper.setTo(new String[]{address});
        // 标题
        mimeMessageHelper.setSubject(subject);
        // 正文
        mimeMessageHelper.setText(body);
        //文件路径
        byte[] bytes = file.getBytes();
        String name = file.getOriginalFilename();
        System.out.println("name = " + name);
        // 添加附件
        mimeMessageHelper.addAttachment(name, new ByteArrayResource(bytes));
        // 发送
        javaMailSender.send(mimeMailMessage);
        return "附件邮件发送成功";
    }

}
```


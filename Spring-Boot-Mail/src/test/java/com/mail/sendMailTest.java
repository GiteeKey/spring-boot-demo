package com.mail;

import com.maizi.mail.MailDemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;

;

@SpringBootTest(classes = MailDemoApplication.class)
@RunWith(SpringRunner.class)
public class sendMailTest {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String account;


    /**
     * 发送简单邮件
     */
    @Test
    public void sendSimpleMail() {
        SimpleMailMessage smm = new SimpleMailMessage();
        // 谁发送
        smm.setFrom(account);
        // 谁接收
        smm.setTo(new String[]{"1228134035@qq.com", "zhaoyi_java@163.com"});
        smm.setSubject("测试发送简单邮件");
        smm.setText("文本内容");
        javaMailSender.send(smm);
    }

    /**
     * 发送附件邮件
     */
    public void sendMail() throws MessagingException {
        MimeMessage mimeMailMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMailMessage, true);
        // 谁发送
        mimeMessageHelper.setFrom(account);
        // 发送日期
        mimeMessageHelper.setSentDate(new Date());
        // 谁接收
        mimeMessageHelper.setTo(new String[]{"1228134035@qq.com"});
        // 标题
        mimeMessageHelper.setSubject("发送附件邮件--》这是标题");
        // 内容
        mimeMessageHelper.setText("发送带有附件的文件--》这是内容");

        //  file使用 MultipartFile 接收使用以下方法
//        byte[] bytes = file.getBytes();
//        String name = file.getName();
//        mimeMessageHelper.addAttachment(name, new ByteArrayResource(bytes));

        javaMailSender.send(mimeMailMessage);

    }

}

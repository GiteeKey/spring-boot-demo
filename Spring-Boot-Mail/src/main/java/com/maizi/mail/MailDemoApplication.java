package com.maizi.mail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class MailDemoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(MailDemoApplication.class, args);
        Environment environment = context.getBean(Environment.class);
        String prot = environment.getProperty("server.port");
        System.out.println("Swagger测试地址：" +
                " http://localhost:" + prot + "/doc.html");
    }

}

package com.maizi.mail.controller;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

@RestController
@RequestMapping("/sendMail")
public class SendMailController {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String account;


    @PostMapping("/simpleMail")
    @ApiOperation("发送简单邮件")
    public String sendSimpleMail() {
        SimpleMailMessage smm = new SimpleMailMessage();
        // 谁发送
        smm.setFrom(account);
        // 谁接收
        smm.setTo(new String[]{"zhaoyi_java@163.com"});
        smm.setSubject("测试发送简单邮件");
        smm.setText("文本内容");
        javaMailSender.send(smm);
        return "简答邮件发送成功";
    }

    /**
     * 发送附件邮件
     */
    @PostMapping("/attachmentMail")
    @ApiOperation("发送带附件的邮件")
    public String sendAttachmentMail(
            @ApiParam("收件邮箱") @RequestParam String address,
            @ApiParam("标题") @RequestParam String subject,
            @ApiParam("正文") @RequestParam String body,
            @ApiParam("附件") @RequestPart MultipartFile file) throws MessagingException, IOException {
        MimeMessage mimeMailMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMailMessage, true);
        // 谁发送
        mimeMessageHelper.setFrom(account);
        // 发送给谁
        mimeMessageHelper.setTo(new String[]{address});
        // 标题
        mimeMessageHelper.setSubject(subject);
        // 正文
        mimeMessageHelper.setText(body);
        //文件路径
        byte[] bytes = file.getBytes();
        String name = file.getOriginalFilename();
        System.out.println("name = " + name);
        // 添加附件
        mimeMessageHelper.addAttachment(name, new ByteArrayResource(bytes));
        // 发送
        javaMailSender.send(mimeMailMessage);
        return "附件邮件发送成功";
    }

}

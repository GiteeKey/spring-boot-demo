package com.maizi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class MongoDBFileApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(MongoDBFileApplication.class, args);
        Environment environment = context.getBean(Environment.class);
        String prot = environment.getProperty("server.port");
        System.out.println("Swagger测试地址：" +
                " http://localhost:" + prot + "/doc.html");

    }
}

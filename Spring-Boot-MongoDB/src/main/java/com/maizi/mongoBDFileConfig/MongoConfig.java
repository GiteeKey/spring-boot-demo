package com.maizi.mongoBDFileConfig;

import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;


@Configuration
public class MongoConfig {

    @Value("${spring.data.mongodb.database}")
    String db;

    /**
     *  如果使用类型注入 报错找不到 MongoClient，测使用 MongoDbFactory 试试
     * Description:
     *
     * Parameter 0 of method getGridFSBucket in com.maizi.mongoBDFileConfig.MongoConfig required a bean of type 'com.mongodb.client.MongoClient' that could not be found.
     *
     * The injection point has the following annotations:
     * 	- @org.springframework.beans.factory.annotation.Autowired(required=true)
     */
//    @Bean
//    public GridFSBucket getGridFSBucket(MongoClient mongoClient) {
//        MongoDatabase database = mongoClient.getDatabase(db);
//        GridFSBucket bucket = GridFSBuckets.create(database);
//        return bucket;
//    }

    @Autowired
    private MongoDbFactory mongoDbFactory;

    @Bean
    public GridFSBucket getGridFSBucket() {
        MongoDatabase database = mongoDbFactory.getDb(db);
        GridFSBucket bucket = GridFSBuckets.create(database);
        return bucket;
    }
}
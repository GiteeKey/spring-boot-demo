package com.maizi.mongoBDFileController;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.maizi.mongoBDFileConfig.FileContentTypeUtils;
import com.maizi.mongoBDFileService.FileService;
import com.maizi.mongoDBFileEntity.FileDocument;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileService fileService;

    /**
     * 视频上传
     *
     * @param file
     * @return
     * @throws IOException
     */
    @ApiOperation("上传文件/视频")
    @PostMapping("/upload")
    public String formUpload(@ApiParam("上传的附件(视频图片等)") @RequestParam("file") MultipartFile file) throws IOException {

        FileDocument fileDocument = fileService.saveFile(file);
        System.out.println(fileDocument);
        return fileDocument.getId();
    }


    /**
     * 在线显示视频
     * 浏览器 http://localhost:9532/file/view/612b3f74d7fa4a3377704c90
     *
     * @param id 文件id
     * @return
     */
    @ApiOperation("根据id在线预览文件")
    @GetMapping("/view/{id}")
    public ResponseEntity<Object> serveFileOnline(@ApiParam("swagger不支持视频，可以在浏览器查看")@PathVariable String id) {
        Optional<FileDocument> file = fileService.getById(id);
        if (file.isPresent()) {
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "fileName=" + file.get().getName())
                    .header(HttpHeaders.CONTENT_TYPE, file.get().getContentType())
                    .header(HttpHeaders.CONTENT_LENGTH, file.get().getSize() + "").header("Connection", "close")
                    .header(HttpHeaders.CONTENT_LENGTH, file.get().getSize() + "")
                    .body(file.get().getContent());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("File was not found");
        }
    }

    /**
     * 列表数据
     * 浏览器 http://localhost:9532/file/list/1/10
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @ApiOperation("查看所有文件")
    @GetMapping("/list/{pageIndex}/{pageSize}")
    public List<FileDocument> list(@ApiParam("第几页") @PathVariable("pageIndex") int pageIndex,
                                   @ApiParam("一页几条数据") @PathVariable("pageSize") int pageSize) {
        return fileService.listFilesByPage(pageIndex, pageSize);
    }

    /**
     * 下载附件
     * 浏览器 http://localhost:9532/file/612b3f74d7fa4a3377704c90
     *
     * @param id 文件id
     * @return
     * @throws UnsupportedEncodingException
     */
    @ApiOperation("根据id下载附件")
    @GetMapping("/{id}")
    public ResponseEntity<Object> downloadFileById(@PathVariable String id) throws UnsupportedEncodingException {
        Optional<FileDocument> file = fileService.getById(id);
        if (file.isPresent()) {
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; fileName=" + URLEncoder.encode(file.get().getName(), "utf-8"))
                    .header(HttpHeaders.CONTENT_TYPE, "application/octet-stream")
                    .header(HttpHeaders.CONTENT_LENGTH, file.get().getSize() + "").header("Connection", "close")
                    .header(HttpHeaders.CONTENT_LENGTH, file.get().getSize() + "")
                    .body(file.get().getContent());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("File was not found");
        }
    }

    /**
     * 根据id删除附件
     *
     * @param id
     * @return
     */
    @ApiOperation("根据id删除附件")
    @GetMapping("/delete/{id}")
    public String deleteFileByGetMethod(@PathVariable("id") String id) {
        fileService.removeFile(id, true);
        return "删除成功";
    }

    /**
     * JS传字节流上传
     *
     * @param md5
     * @param ext
     * @param request
     * @param data
     * @return
     * @throws Exception
     */
    @ApiOperation("JS传字节流上传")
    @Deprecated
    @PostMapping("/upload/{md5}/{ext}")
    public String jsUpload(@PathVariable String md5, @PathVariable String ext, HttpServletRequest request, @RequestBody byte[] data) throws Exception {

        if (StrUtil.isEmpty(md5)) {
            return "请传入文件的md5值";
        }
        if (!StrUtil.isEmpty(ext) && !ext.startsWith(".")) {
            ext = "." + ext;
        }

        String name = request.getParameter("name");
        String description = request.getParameter("description");
        InputStream in = new ByteArrayInputStream(data);
        System.out.println("data_string:" + StrUtil.str(data, "UTF-8"));
        if (in != null && data.length > 0) {
            FileDocument fileDocument = new FileDocument();
            fileDocument.setName(name);
            fileDocument.setSize(data.length);
            fileDocument.setContentType(FileContentTypeUtils.getContentType(ext));
            fileDocument.setUploadDate(new Date());
            fileDocument.setSuffix(ext);
            String fileMd5 = SecureUtil.md5(in);
            fileDocument.setMd5(fileMd5);
            System.out.println(md5 + " , " + fileMd5);
            fileDocument.setDescription(description);
            fileService.saveFile(fileDocument, in);
            System.out.println(fileDocument);
            in.close();
            return "上穿成功";
        }
        return "上次失敗";
    }

}
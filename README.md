# SpringBoot-Demo

#### 介绍
学习Spring-Boot

我的CSDN博客： [https://blog.csdn.net/weixin_46237429](https://blog.csdn.net/weixin_46237429)

我的个人博客：[一粒麦子](http://mblog.vaiwan.com/)



[Spring-Boot-Mail: 使用SpringBoot发送邮件](https://blog.csdn.net/weixin_46237429/article/details/120111637?spm=1001.2014.3001.5501)

[Spring-Boot-MongoDB：(MongoDB，存储文件/视频)](https://blog.csdn.net/weixin_46237429/article/details/120948950?spm=1001.2014.3001.5501)

[Spring-Boot-QRcode： SpringBoot集成二维码](https://blog.csdn.net/weixin_46237429/article/details/111150475?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163565510616780271547584%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=163565510616780271547584&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~blog~first_rank_v2~rank_v29-1-111150475.pc_v2_rank_blog_default&utm_term=%E4%BA%8C%E7%BB%B4%E7%A0%81&spm=1018.2226.3001.4450)

[Spring-Boot-Excel：easyExcel读写操作](https://blog.csdn.net/weixin_46237429/article/details/121062451)

[Spring-Boot-Rabbitmq: 生产者消费者确认/重试/延迟发送]()

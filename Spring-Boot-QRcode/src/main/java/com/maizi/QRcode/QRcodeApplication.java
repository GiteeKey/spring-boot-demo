package com.maizi.QRcode;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@Slf4j
@SpringBootApplication
public class QRcodeApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(QRcodeApplication.class);
        String port = context.getEnvironment().getProperty("server.port");
        log.info("访问地址 http://localhost:" + port+"/qrCode/generator?codeContent=https://www.baidu.com");
    }
}
package com.maizi.QRcode.controller;

import com.maizi.QRcode.utils.QRCodeUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/qrCode")
public class QRCodeGeneratorController {

    @Resource
    HttpServletResponse response;

    @GetMapping("/generator")
    public void encodeQrCode(@RequestParam("codeContent") String codeContent) {
        // 嵌入二维码的图片路径(经测试，这必须是本地图片，不能是网络图片url地址)
//        String imgPath = "C:\\Users\\windows\\Desktop\\R-C.jpg";
        String imgPath = getClass().getClassLoader().getResource("picture/timg.jpeg").getPath();
        System.out.println("imgPath = " + imgPath);
        try {
            QRCodeUtil.encode(codeContent, imgPath, true, response.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}